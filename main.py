import pymongo
from flask import Flask,render_template

app = Flask(__name__)

client = pymongo.MongoClient("mongodb://localhost:27017/")

mydb = client["PPAP_OEE"]

mycol = mydb["production_orders"]


@app.route('/home',methods=['GET', 'POST'])
def my_func():
    data = [i for i in mycol.find()]
    return render_template('ui.html', mydata=data)

app.run(debug=True)

